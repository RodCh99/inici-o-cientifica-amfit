# Amfit

Aplicação Móvel de Fiscalização de Transito. É uma pesquisa no modelo do Programa de<br>
Iniciação científica em desenvolvimento Tecnológico e Inovação (P.I.B.I.C.T.I) realizada no<br> 
centro univrsitario UniCEUB pelo aluno Rodrigo Pereira da rocha sob orientações do prof<br>
phd Paulo Rogério Foina

# Introdução do Projeto de pesquisa:

![descrição](screenshots/introdução_do_pic.png)

# Estrutura do Projeto:

![imagem inicial](screenshots/rest.png)

**Descrição:**

Quando uma delito for identificada pelo usuario, o app é capaz de capturar a imgem da infração <br>
juntamente com dados de geolocalização (latitude e logitude), horário e uma classifcação da vio-<br>
lação feita preenchida em formulario pelo utilizador. Em seguida, os dados referentes são  envia-<br>
dos para um banco de dados NoSQL com funcionamento em tempo real provido pela plataforma <br>
de Saas (backend as a service) do Google chamada firebase. <br><br>
Por fim, foi desenvolvida uma API REST que consome o banco de dados disponibiliza os dados pa-<br>
ra cosulta publica, incentivando que os órgãos competentes como DETRAN'S, DER e ministério<br>
capturem as informações e as utilizem para aprimorar seus processos de captação de denuncias,<br>
anexando seus sistemas web a API desenvolvida.   

# Como este repositório está organizado?

Eles está divido em 3 pastas principais, sendo elas: AmfitApp que contem o código react-native ou<br>
seja, em javascript com diversas bibliotecas de extensão seja para camera como react-natie-camera<br>
ou para acesso ao firebase com react-native-firebase. Já a segunda pasta concentra os códigos da <br>
linguagem python da API REST com ajuda do SWAGGER para criação de uma interface visual na web<br>
básica. Completando, em screenshots éé possivel encontrar imagens das aplicaçõões e conteudos<br>
relacionados

# Como Rodar:

### 1º) **Aplicação Mobile:**

- **É necessário possuir o Node instalado:** disponivel para download [aqui](https://nodejs.org/en/download/)
- **Junto com o Node é necessário o NPM:**   também para download [aqui](https://nodejs.org/en/download/)
- **Como alternativa ao NPM existe o yarn:**  disponivel para instalação [aqui](https://yarnpkg.com/)


> git clone https://gitlab.com/RodCh99/inici-o-cientifica-amfit/-/tree/master.git <br><br>
npm install         // ou yarn add<br><br>
react-native start

### 2º) **Aplicação Web:**
- **É necessário possuir o Python 3 instalado:** disponivel para download [aqui](https://www.python.org/)
- **Junto com o Python é necessário o PIP:** também para dowload [aqui](https://www.python.org/)
- **São necessária diversas bibliotecas que devem ser instaladas com o PIP** 

> python app.py
