# Importando as bibliotecas para a aplicação:
from flask import Flask

from flask_restplus import Api, Resource

from db_config import db

App = Flask(__name__)
Api = Api(app=App, version="1.0", title="Api Denúncias", description="<strong>API do projeto de Iniciação cientifica de Inovação e Tecnologia A.M.F.I.T<strong>")

name_space = Api.namespace('infracoes', description='Campo relacionado as infrações denunciadas via App')


"""

    1º) SALVAR DADOS COM FIREBASE: --- https://firebase.google.com/docs/database/admin/save-data
    
    2º) RECUPERAR DADOS COM FIREBASE: --- https://firebase.google.com/docs/database/admin/retrieve-data
    
    3°) BIBLIOTECA DO FIREBASE PARA PYTHON: --- https://firebase.google.com/docs/admin/setup
    
    4º) REFERENCIA NO FIREBASE: --- https://firebase.google.com/docs/reference/js/firebase.database.Reference
    
    5º) EFERENCIA FINAL PARA TERMINAR A API --- https://firebase.googleblog.com/2017/07/accessing-database-from-python-admin-sdk.html
    
"""


@name_space.route("/")
class MainClass(Resource):

   def get(self):

       output = []

       #denuncias = dict(db.child("Denuncias").get().val()).values()

       denuncias = dict(db.child("Denuncias").order_by_child('dataDenuncia').get().val()).values()

       for den in denuncias:

           del den['UidUsuario']

           output.append(den)

       return output

@name_space.route('/<int:fim>/<int:inicio>')
class SubClass(Resource):
    
    def get(self, inicio, fim):

        """
            :param inicio (int): formato YYYYmmdd\n
            :param fim (int) : formato YYYYmmdd\n
            :return denuncias(array): denuncias filtradas
        """

        if (len(str(inicio)) or len(str(fim))) > 8:

            return {'erro': 'você inseriu dados a mais que o necessário!'}

        elif (len(str(inicio)) or len(str(fim))) < 8:

            return {'erro': 'você inseriu dados a menos que o necessário!'}

        elif (len(str(inicio)) and len(str(fim))) == 8:

            # print(f'DENUNCIAS -- { denuncias }')

            if inicio >= fim:

                return {'erro': 'você inseriu dados em ordem incorreta!'}

            else:

                output = []

                try:

                    #print('Passou em Try')

                    denuncias = dict(db.child("Denuncias")
                                     .order_by_child("dataDenuncia")
                                     .start_at(inicio)
                                     .end_at(fim)
                                     .get().val()).values()

                    #print(f'Denuncias --- { denuncias }')

                    for den in denuncias:

                        del den['UidUsuario']

                        output.append(den)

                    return output

                except:

                    #print('veio para a excessão')
                    output = "data não encotrada"

                    return output


# Rodando a aplicação flask
if __name__ == "__main__":
   App.run()
