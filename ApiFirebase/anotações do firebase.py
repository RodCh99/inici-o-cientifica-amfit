"""
    Firebase v3 Query by Grandchild:

        - https://firebase.google.com/docs/admin/setup

    Accessing Database from the Python Admin SDK:

        - https://firebase.googleblog.com/2017/07/accessing-database-from-python-admin-sdk.html

    Firebase v3 Query by Grandchild

        - https://stackoverflow.com/questions/38515142/firebase-v3-query-by-grandchild

    Firebase python

        - https://firebase.google.com/docs/reference/admin/python/firebase_admin.db

    “error”: “Index not defined, add ”.indexOn"

        - https://stackoverflow.com/questions/34968413/error-index-not-defined-add-indexon

    Firebase setup

        - https://firebase.google.com/docs/admin/setup/

"""